from flask import Flask
from flask import request
import urllib
import json
import os
app = Flask(__name__)


@app.route('/')
def querydistrict():
    token = os.environ.get('APP_TOKEN')
    request_url = 'https://data.austintexas.gov/resource/tc36-hn4j.json?$$app_token=' + token
    r = urllib.request.urlopen(request_url)
    data = json.loads(r.read())

    districtNum = request.args.get('district')
    computersNum = request.args.get('computers')
    wifi = request.args.get('wifi')
    params = {"district": districtNum, "computers": computersNum, "wifi": wifi}
    filtered = dict(filter(lambda item: item[1] is not None, params.items()))
    if len(filtered) == 0:
        message = [{"welcome_message": "Welcome to Austin Libraries Information Web Application"}]
        json_message = json.dumps(message)
        return json_message
    else:
        datas=[]
        for k, v in filtered.items():
            for item in data:
                if k in item:
                    if item[k] == v:
                        datas.append(item)
           # data = [item for item in data if item[k] == v]

        output = []
        for item in datas:
            temp = {}
            for k, v in item.items():
                if k == "phone":
                    p_info = {k: v}
                if k == "name":
                    n_info = {k: v}
                if k == "address":
                    for k2, v2 in v.items():
                        if k2 == "human_address":
                            a_info = {k2: v2}
            temp.update(n_info)
            temp.update(p_info)
            temp.update(a_info)
            output.append(temp)
        return json.dumps(output)
